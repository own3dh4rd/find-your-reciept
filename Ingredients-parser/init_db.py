import os
import sys
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import ingredients


Base = declarative_base()
 
class Category(Base):
    __tablename__ = 'category'
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)


    def __init__(self, name):
        self.name = name


    def __repr__(self):
        return "Category name %s" % (self.name)


 

class Product(Base):
    __tablename__ = 'product'
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    filename = Column(String(250), nullable=True)
    category_id = Column(Integer, ForeignKey('category.id')) 
    category = relationship(Category)


    def __init__(self, name, filename, category):
        self.name = name
        self.filename = filename
        self.category = category


    def __repr__(self):
        return "Product name, img %s" %(self.filename)




if __name__ == "__main__":
    engine = create_engine('postgresql+psycopg2://postgres:glotka@localhost/postgres')
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    session = Session()



    def init_categories():
        for category in ingredients.ing_categories:
            if session.query(Category).filter_by(name=category).first():
                continue
            session.add(Category(category))
            print "Category added: %s" % (category)
            session.commit()


    def init_products():
        idx = {} #cached categories
        for category in session.query(Category).all():
            idx[category.name] = category

        parser = ingredients.IngredientsParser()
        for data in parser:
            for key in data:
                if session.query(Product).filter_by(name=key[0]).first():
                    continue
                session.add(Product(name=key[0], category=idx[key[1]], filename=data[key]))
                print "Product added: %s" % (key[0])
                session.commit()


    init_categories()
    init_products()
    print "Count categories in database: %s" % (session.query(Category).count())
    print "Count products in database: %s" % (session.query(Product).count())
    session.close()