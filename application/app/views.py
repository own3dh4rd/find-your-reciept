#coding: utf-8
from app import app, db, model
from flask import render_template, jsonify, request, abort



def _filter(recipes, products):
	"""Функция, отбирающая те рецепты, в которых есть все продукты из products"""
	result = []
	for recipe in recipes:
		temp = [i.mainproduct.name for i in recipe.ingredients.all()]

		for product in products:
			if product not in temp:
				flag = False
				break
			else:
				flag = True


		if not flag:
			continue
		else:
			result.append(recipe)

	return result




def get_recipes(products, mode=0):
	"""Фукнция поиска рецептов"""
	recipes = set()

	if not products:
		return recipes


	for product in products:
		pr = db.session.query(model.Product).filter_by(name = product).first()

		if not pr:
			print "%s is not exists" % (product)
			continue

		print "%s was found" % (pr.name)
		pr_ext = db.session.query(model.ExtProduct).filter_by(product_id = pr.id).all()
		for x in pr_ext:
			for y in x.recipes.all():
				recipes.add(y)


	if mode and recipes and len(products) > 1:
		#mode 1 - поиск по включению(все продукты должны быть в рецепте)
		recipes = _filter(recipes, products)

	return recipes





@app.route('/')
@app.route('/index')
def index():
	return render_template('index.html')



@app.route('/recipes', methods=["POST"])
def return_recipes():
	"""
		GET /recipes - запрос на поиск рецептов по заданным ингредиентам. 
		Возвращает список рецептов в базе
	"""
	if request.method == 'POST':
		if request.json:
			args = request.json.get('ingredients', [])
			mode = request.json.get('mode', 0)
		else:
			abort(404)

		if not args:
			return jsonify({})

		print "Args from client: %s. Mode: %s" % (args, mode)
		recipes = get_recipes(args, int(mode))
		
		result = {}
		result['recipes'] = []
		for recipe in recipes:
			result['recipes'].append(recipe.serializable())
		return jsonify(result)



@app.route('/recipe/<int:id>', methods=['GET'])
def return_recipe(id):
	"""
		GET /recipe/id - запрос на рецепт с recipe.id = id
		Возвращает json с рецептом
	"""
	if request.method == 'GET':
		recipe = db.session.query(model.Recipe).filter_by(id = id).first()

		if recipe:
			return jsonify(recipe.serializable())

		return jsonify({})





@app.route('/autocomplete', methods=['POST'])
def get_products():
	if request.method == "POST":
		term = request.form['name']

		print "Term is %s" % (term)

		data = []
		result = db.session.query(model.Product).filter(model.Product.name.like(term + '%')).all()
		for x in result:
			data.append(x.name)

		for x in data:
			print x

		return jsonify({'names': data})

