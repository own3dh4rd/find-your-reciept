import ingredients
import recipes
from app import model, db






if __name__ == "__main__":
    db.drop_all()
    db.create_all()



    def init_categories():
        print "Start parse and init CATEGORIES AND TAGS..."
        for category in ingredients.ing_categories:
            if db.session.query(model.Category).filter_by(name=category).first():
                continue
            db.session.add(model.Category(name=category))
            db.session.commit()
            print "\tCategory of product was added: %s" % (category)

        print "Categories of products was added: %s" % db.session.query(model.Category).count()

        for category in recipes.categories:
            if db.session.query(model.CategoryRecipe).filter_by(name=category).first():
                continue
            c = model.CategoryRecipe(name=category)
            db.session.add(c)
            db.session.commit()
            print "\tCategory of recipe was added: %s" % (category)

        print "Categories of recipes was added: %s" % db.session.query(model.CategoryRecipe).count()

        for tag in recipes.tags:
            if db.session.query(model.RecipeTags).filter_by(name=tag).first():
                continue

            t = model.RecipeTags(name=tag)
            db.session.add(t)
            db.session.commit()
            print "\tTag was added: %s" % (tag)
        
        print "Categories of recipes was added: %s" % db.session.query(model.RecipeTags).count()
        print "End init."


    def init_products():
        print "Start parse and init PRODUCTS..."
        idx = {} #cached categories
        for category in db.session.query(model.Category).all():
            idx[category.name] = category


        parser = ingredients.IngredientsParser()
        for data in parser:
            if db.session.query(model.Product).filter_by(name=data[0]).first():
                continue

            new_p = model.Product(name=data[0], image=data[2])
            db.session.add(new_p)
            new_p.follow(idx[data[1]])
            db.session.add(new_p)

            print "\tProduct was added: %s" % (data[0])
            db.session.commit()
        print "End init."


    def init_recipes():
        print "Start parse and init RECIPES..."
        count = 0
        parser = recipes.RecipeParser()

        for data in parser:
            for key in data:
                #type(key) is tuple
                recipe_name = key[0]
                category = key[1]

                exists_category = db.session.query(model.CategoryRecipe).filter(model.CategoryRecipe.name == category).first()


                for d in data[key]:
                    for i in d:
                        if i == 'steps':
                            steps = d[i]
                        elif i == 'portions':
                            portions = d[i]
                        elif i == 'image':
                            image = d[i]
                        elif i == 'cookingTime':
                            cooking_time = d[i]
                        elif i == 'ingredients':
                            ingredients = []
                            for name in d[i]:

                                w = d[i][name]

                                exists_product = db.session.query(model.Product).filter(model.Product.name == name).first()

                                if not exists_product:
                                    exists_product = model.Product(name=name, image='')
                                    db.session.add(exists_product)
                                    db.session.commit()

                                ext_product = model.ExtProduct(w)
                                ext_product.mainproduct = exists_product

                                db.session.add(exists_product)
                                db.session.add(ext_product)
                                db.session.commit()

                                ingredients.append(ext_product)

                        elif i == 'tags':
                            tags = []
                            for t in d[i]:
                                exists_tag = db.session.query(model.RecipeTags).filter(model.RecipeTags.name == t).first()
                                if not exists_tag:
                                    exists_tag = model.RecipeTags(name=t)
                                    db.session.add(exists_tag)
                                    db.session.commit()

                                tags.append(exists_tag)

                r = model.Recipe(name=recipe_name, steps=steps)
                r.cookingTime = cooking_time
                r.portions = portions
                r.category = exists_category
                r.image = image
                for ing in ingredients:
                    r.follow_product(ing)
                    db.session.add(r)
                    db.session.commit()

                for tag in tags:
                    r.follow_tag(tag)
                    db.session.add(r)
                    db.session.commit()
                db.session.add(r)
                db.session.commit()

            count += 1

        print "End init."


    init_categories()
    init_products()
    init_recipes()
    db.session.close()